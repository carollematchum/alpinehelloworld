# alpinehelloworld-gitlab-ci

## Description

This repository is used to demonstrate how to use .gitlab-ci.yml to build pipeline

## Tools using
  - gitlab version control and source code management. It allows multiple developers to collaborate on projects, track changes, and manage code repositories.
  - .gitlab-ci define workflows to automate your CI/CD pipeline. These workflows include steps  build, test acceptation, release, review and deploying application.
  - Docker used to containerize applications, making them portable and ensuring consistency between development and production environments.



